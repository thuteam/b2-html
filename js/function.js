$(function () {
  $('.p-bar').each(function () {
    var $this = $(this);
    var width = $this.data('rate');
    $this.find('span').css({'width': width + '%'});
  });

  $('.timer-down').each(function () {
    var $this = $(this);

    if ($this.data('timer')) {

      var x = setInterval(function () {

        var endTime = $this.data('timer');

        var now = new Date();
        var distance = endTime - now.getTime();


        var minutes = Math.floor(distance / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        var minisec = Math.floor((distance % (10 * 60)) / 10);


        $innerHtml = '<abbr>' + minutes + ':' + seconds + ':' + minisec + '</abbr>';
        $this.html($innerHtml);

        if (distance < 0) {
          clearInterval(x);
          $this.addClass('expred');
          $innerHtml = '<abbr>EXPIRED</abbr>';
          $this.html($innerHtml);
        }
      }, 10);
    }
  });

  $('.n-picture').each(function () {
    var $this = $(this);

    $this.find('.colorandpic .item').click(function () {
      var $item = $(this);
      $this.find('.colorandpic .item').removeClass('selected');
      $item.addClass('selected');

      var newImage = $item.find('img').attr('src');

      $this.find('.viewing img').attr('src', newImage);
    });
  });

  $('.fast-select').each(function () {
    var $this = $(this);
    $this.find('a').click(function () {
      var $link = $(this);
      $this.find('a').removeClass('selected');
      $link.addClass('selected');

      $this.closest('.snum').find('.value').val($link.data('value'));
    });
  });

  $('.spinner').each(function () {
    var $this = $(this);
    $this.find('.decrease').click(function () {
      var $value = $this.find('.value');
      if ($value.val() <= 1) {
        $value.val(1);
      } else {
        $value.val(parseFloat($value.val()) - 1);
      }
    });

    $this.find('.increase').click(function () {
      var $value = $this.find('.value');
      if ($value.val() <= 0) {
        $value.val(1);
      } else {
        $value.val( parseFloat($value.val()) + 1);
      }
    });
  });

  $('.c-dao .timer').each(function () {
    var $this = $(this);

    if ($this.data('timer')) {

      var d= new Date();
      d.setDate(d.getDate() + 2);

      var x = setInterval(function () {

        var endTime = $this.data('timer');

        var now = new Date();
        var distance = endTime - now.getTime();

        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        $innerHtml = '<abbr>'+ hours + ':' + minutes + ':' + seconds + '</abbr>';
        $this.html($innerHtml);

        if (distance < 0) {
          clearInterval(x);
          $this.addClass('expred');
          $innerHtml = '<abbr>EXPIRED</abbr>';
          $this.html($innerHtml);
        }
      }, 1000);
    }
  });

  $('.rating .rating-percent').each(function(){
    var $this = $(this);
    var percent = $this.data('rating');

    $this.find('span').css({'width': percent + '%'});
  });
});